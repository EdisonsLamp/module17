
#include <iostream>
#include <math.h>

class Vector
{
private:
	double x = 0;
	double y = 0;
	double z = 0;

public:
	Vector() : x(2), y(3), z(3)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	double Module(double a, double b, double c)
	{
		a *= a;
		b *= b;
		c *= c;
		double q = a + b + c;
		return sqrt(q);
	}

	void Show()
	{
		std::cout << x << " " << y << " " << z << '\n';
	}
	
};

int main()
{
	Vector x(45.5, 32.5, 56.4);
	x.Show();
	std::cout << "Vector's Length is " << x.Module(12,67,45) << std::endl;
	x.Show();
	return 0;
}